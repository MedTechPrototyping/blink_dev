#include <Arduino.h>

/* Task:

Blink 2 LEDs (D13, D10), at indpendent frequencies

*/

/* D pins can just be specified by numbers */
#define LED1_PIN 13
#define LED2_PIN 10
#define LED1_BLINK_FREQ_HZ 5
#define LED2_BLINK_FREQ_HZ 10
#define LED1_DUTY_CYCLE_PCT_ON 50
#define LED2_DUTY_CYCLE_PCT_ON 50
#define VOLT_LOGIC_MV 3300
#define LED1_START_STATE 0
#define LED2_START_STATE 0

#define LED1_ON_TIME_MS (LED1_DUTY_CYCLE_PCT_ON*10)/LED1_BLINK_FREQ_HZ
#define LED1_OFF_TIME_MS ((100-LED1_DUTY_CYCLE_PCT_ON)*10)/LED1_BLINK_FREQ_HZ
#define LED2_ON_TIME_MS (LED2_DUTY_CYCLE_PCT_ON*10)/LED2_BLINK_FREQ_HZ
#define LED2_OFF_TIME_MS ((100-LED2_DUTY_CYCLE_PCT_ON)*10)/LED2_BLINK_FREQ_HZ

/* Function Declarations */
void turn_led_on(void);

/* Global Variables */
unsigned long current_time_ms = 0;
unsigned long led1_next_on_time_ms;

/* Create a struct "template" */
struct led {
    int pin;
    int blink_freq;
    int duty_cycle_pct_on;
    bool state;
};

void setup() {
    pinMode(LED1_PIN, OUTPUT);
    pinMode(LED2_PIN, OUTPUT);

    Serial.begin(9600);

    led1_next_on_time_ms = LED1_ON_TIME_MS;
}


void loop() {
    current_time_ms = millis();

    /* When do you do this? */
    turn_led_off();

    if (current_time_ms >= led1_next_on_time_ms) {
        turn_led_on();
    }
}

// TODO: refactor to take pin input
void turn_led_on(void) {
        digitalWrite(LED1_PIN, 1);
        Serial.println("Turned LED1 on:");
        Serial.println(current_time_ms);
}